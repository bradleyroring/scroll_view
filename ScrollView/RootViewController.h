//
//  RootViewController.h
//  ScrollView
//
//  Created by Brad Roring on 9/1/17.
//  Copyright (c) 2017 Brad Roring. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RootViewController : UIViewController <UIPageViewControllerDelegate>

@property (strong, nonatomic) UIPageViewController *pageViewController;

@end

