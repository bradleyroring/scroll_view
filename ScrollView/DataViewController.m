//
//  DataViewController.m
//  ScrollView
//
//  Created by Brad Roring on 9/1/17.
//  Copyright (c) 2017 Brad Roring. All rights reserved.
//

#import "DataViewController.h"

@interface DataViewController ()
@end

@implementation DataViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIScrollView* scrollview = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];//Initalizing scrollview memory and allocating, and setting frame
    scrollview.contentSize = CGSizeMake(self.view.frame.size.width * 3, self.view.frame.size.height*3);
    scrollview.backgroundColor = [UIColor blackColor];
    [scrollview setPagingEnabled:YES];
    [self.view addSubview:scrollview];
    
    UIView* view0 = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    view0.backgroundColor = [UIColor yellowColor];
    [scrollview addSubview:view0];
    
    UIView* view1 = [[UIView alloc]initWithFrame:CGRectMake(self.view.frame.size.width, 0, self.view.frame.size.width, self.view.frame.size.height)];
    view1.backgroundColor = [UIColor redColor];
    [scrollview addSubview:view1];
    
    UIView* view2 = [[UIView alloc]initWithFrame:CGRectMake(self.view.frame.size.width*2, 0, self.view.frame.size.width, self.view.frame.size.height)];
    view2.backgroundColor = [UIColor blueColor];
    [scrollview addSubview:view2];
    
    UIView* view3 = [[UIView alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height)];
    view3.backgroundColor = [UIColor purpleColor];
    [scrollview addSubview:view3];
    
    UIView* view4 = [[UIView alloc]initWithFrame:CGRectMake(self.view.frame.size.width, self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height)];
    view4.backgroundColor = [UIColor orangeColor];
    [scrollview addSubview:view4];
    
    UIView* view5 = [[UIView alloc]initWithFrame:CGRectMake(self.view.frame.size.width*2, self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height)];
    view5.backgroundColor = [UIColor whiteColor];
    [scrollview addSubview:view5];
    
    UIView* view6 = [[UIView alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height*2, self.view.frame.size.width, self.view.frame.size.height)];
    view6.backgroundColor = [UIColor brownColor];
    [scrollview addSubview:view6];
    
    
    UIView* view7 = [[UIView alloc]initWithFrame:CGRectMake(self.view.frame.size.width, self.view.frame.size.height*2, self.view.frame.size.width, self.view.frame.size.height)];
    view7.backgroundColor = [UIColor grayColor];
    [scrollview addSubview:view7];
    
    UIView* view8 = [[UIView alloc]initWithFrame:CGRectMake(self.view.frame.size.width*2, self.view.frame.size.height*2, self.view.frame.size.width, self.view.frame.size.height)];
    view8.backgroundColor = [UIColor greenColor];
    [scrollview addSubview:view8];
}

-(BOOL)thisCoolThing:(BOOL)returnType withSomething:(int)number {
    return returnType;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
