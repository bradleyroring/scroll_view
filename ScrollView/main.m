//
//  main.m
//  ScrollView
//
//  Created by Brad Roring on 9/1/17.
//  Copyright (c) 2017 Brad Roring. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
